﻿using UnityEngine;
using System.Collections;

//add this to use the List
using System.Collections.Generic;


public class playerController : MonoBehaviour {
	GameObject cursor;

	//add this to create a list of positions
	public List<Vector3> positionsToTraverse;

	mouseController mController;

	// Use this for initialization
	void Start () {

		positionsToTraverse = new List<Vector3> ();

		cursor = GameObject.FindGameObjectWithTag ("cursor");

		mController = GameObject.Find ("MouseCursor").GetComponent<mouseController> ();
	}




	// Update is called once per frame
	void Update () {

		//this is the difference between the rotations of the two objects
		Vector3 rotationalDifference = cursor.gameObject.transform.position - transform.position;

		//this is to get the difference in the rotations so it points towards the mouse cursor
		float rotationInZ = Mathf.Atan2 (rotationalDifference.y, rotationalDifference.x) * Mathf.Rad2Deg;

		//this sets the rotation accordingly

		if (!mController.isMoving)
			transform.rotation = Quaternion.Euler (0f, 0f, rotationInZ);

	
	}
}
