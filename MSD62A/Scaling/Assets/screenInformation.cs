﻿using UnityEngine;

using UnityEngine.UI;

using System.Collections;

public class screenInformation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//show some information about the screen
		string screenInfo1 = Screen.width + " X " + Screen.height;

		screenInfo1 += "\nCamera size: " + Camera.main.orthographicSize;

		screenInfo1 += "\nAspect ratio: " + Camera.main.aspect;

		screenInfo1 += "\nSquare position(world): " + GameObject.Find ("square").GetComponent<Transform> ().position;

		Vector3 squarePos = Camera.main.WorldToScreenPoint (GameObject.Find ("square").GetComponent<Transform> ().position);

		screenInfo1 += "\nSquare position(pixels): " + squarePos;

		//the text component is attached to THIS object
		GetComponent<Text> ().text = screenInfo1;

	
	}
}
