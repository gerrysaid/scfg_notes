﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class keyboardController : MonoBehaviour {
	public float speed;

	float limitX;
	float limitY;

	//public int so we can set it in Unity if we want more lives to start with
	public int lives;

	public int countDown;

	//a reference to the health prefab we are going to spawn
	public GameObject health;


	string[] spawnLocations = new string[3];



	IEnumerator spawnHealth(){
		spawnLocations [0] = "Location1";
		spawnLocations [1] = "Location2";
		spawnLocations [2] = "Location3";

		while(true){
			//choose a number from 0-2
			int randomLocation = Random.Range(0,3);
			//write it in the console
			Debug.Log(randomLocation);
			//if there is a health prefab on the screen, destroy it to create a new one
			if (GameObject.FindGameObjectWithTag("healthPowerup")!=null)
				Destroy(GameObject.FindGameObjectWithTag("healthPowerup"));
			//find one of the random locations by name
			Vector3 tempPosition = GameObject.Find(spawnLocations[randomLocation]).transform.position;
			//create an instance of the prefab in one of the temporary positions
			Instantiate(health,tempPosition,Quaternion.identity);
			//wait one second
			yield return new WaitForSeconds (1f);
		} 
	}
		
	// Use this for initialization
	void Start () {
		//if not set in Unity, the value is 3
		if (lives == 0)
			lives = 3;

		if (countDown == 0)
			countDown = 60;

		//set the text in the lives window to the value in the script
		GameObject.Find ("Lives Text").GetComponent<Text> ().text = "Lives: " + lives;
		
		//place the object in the middle of the screen
		transform.position = new Vector3 (-8f, -4f);

		//to start the timer
		StartCoroutine(Timer());
		StartCoroutine (spawnHealth ());
	}

	IEnumerator Timer()
	{
		//countdown stops after 60 seconds by default
		while (countDown > 0) {
			//sets the text of the countdown if the countdown is greater than 10
			GameObject.Find ("Timer Text").GetComponent<Text> ().text = "00: " + countDown;
			//if the countdown is less than 10 I need to add a zero in front of the number
			if (countDown < 10) {
				//add a zero in front of the single number
				GameObject.Find ("Timer Text").GetComponent<Text> ().text = "00: " + "0"+countDown;
			}
			//reduce the countdown by 1
			countDown--;
			yield return new WaitForSeconds (1f);
		}
		yield return null;
	}

	//show the win text for five seconds
	IEnumerator Win()
	{
		GameObject.Find ("WinText").GetComponent<Text> ().enabled = true;
		yield return new WaitForSeconds (5f);
		GameObject.Find ("WinText").GetComponent<Text> ().enabled = false;
	}


	//show the lose text for five seconds
	IEnumerator Lose()
	{
		GameObject.Find ("LoseText").GetComponent<Text> ().enabled = true;
		yield return new WaitForSeconds (5f);
		GameObject.Find ("LoseText").GetComponent<Text> ().enabled = false;
	}

	void OnTriggerEnter2D(Collider2D otherObject)
	{
		//hit the health object
		if (otherObject.gameObject.tag == "healthPowerup") {
			//increase lives by 1
			lives++;
			//update the text in the top left corner
			GameObject.Find ("Lives Text").GetComponent<Text> ().text = "Lives: " + lives;
			//destroy the lives powerup
			Destroy (otherObject.gameObject);
		}
	}







	void OnCollisionEnter2D(Collision2D hit)
	{
		//check if I hit the wall
		if (hit.collider.gameObject.tag == "wall") {
			lives--;

			if (lives == 0) {
				//restart game, show game over text etc...
				transform.position = new Vector3 (-8f, -4f);
				//reset lives to 3
				lives = 3;
				//stop the timer
				StopAllCoroutines ();
				//reset the countdown
				countDown = 60;
				//start the timer
				StartCoroutine (Timer ());
				StartCoroutine (Lose ());
			}

			GameObject.Find ("Lives Text").GetComponent<Text> ().text = "Lives: " + lives;

		}

		if (hit.collider.gameObject.name == "Ending") {
			//if you get to the exit of the maze, you go back to the start
			StartCoroutine(Win());

			//transform.position = new Vector3 (-8f, -4f);
			//if I am in level 1, take me to level 2
			if (SceneManager.GetActiveScene().name == "level1")
				SceneManager.LoadScene("level2");


		}
	}

	bool stopstart = false;


	// Update is called once per frame
	void FixedUpdate () {

		//stop the animation
		if (Input.GetKeyDown (KeyCode.Space)) {
			//stop the animation
			if (stopstart == false) {
				GameObject.Find ("AnimatedObstacle").GetComponent<Animator> ().speed = 0f;
				stopstart = true;
			} else {
				GameObject.Find ("AnimatedObstacle").GetComponent<Animator> ().speed = 1f;
				stopstart = false;
			}
			Debug.Log(stopstart+"stop/start");
		}


		//reset position when the time is up
		if (countDown == 0) {
			transform.position = new Vector3 (-8f, -4f);
			StopAllCoroutines ();
			countDown = 60;
			lives = 3;
			StartCoroutine (Timer ());
			StartCoroutine (Lose ());
		}

		
		//size variable inside the camera
		limitY = Camera.main.orthographicSize;

		limitX = Camera.main.orthographicSize * Camera.main.aspect;

		limitX = limitX - (transform.localScale.x / 2);
		limitY = limitY - (transform.localScale.y / 2);


		//keep the box on the screen
		transform.position = new Vector3 (
			Mathf.Clamp (transform.position.x, -limitX, limitX),
			Mathf.Clamp (transform.position.y, -limitY, limitY),
			0f);
		

	//	Debug.Log(Input.GetAxis("Horizontal") + " " + Input.GetAxis("Vertical"));

		//move the object horizontally
		transform.Translate (new Vector3 (-1f, 0f) * speed * Input.GetAxis ("Horizontal") * Time.deltaTime);

		//vertically
		transform.Translate (new Vector3 (0f, 1f) * speed * Input.GetAxis ("Vertical") * Time.deltaTime);
	
	}
}
