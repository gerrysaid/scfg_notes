﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//sqlite dlls so that I can connect to the database
using Mono.Data.Sqlite; 
using System.Data; 
using System;



public class dbScript : MonoBehaviour {


	IDbConnection dbconn;

	// Use this for initialization
	void Start () {
		
		getData ();

	}

	public void getData(){
		//we connect to the database here.  The application.datapath is the assets folder
		string conn = "URI=file:" + Application.dataPath + "/highscores.s3db"; //Path to database.

		Debug.Log (conn);
		//an instance reference of the database connection

		//create an instance of the connection
		dbconn = (IDbConnection) new SqliteConnection(conn);

		dbconn.Open(); //Open connection to the database.
		//create a prepared command
		IDbCommand dbcmd = dbconn.CreateCommand();

		//generate the query
		string sqlQuery = "SELECT id,playername,score FROM scores ORDER BY score DESC";
		//set the command text of the query
		dbcmd.CommandText = sqlQuery;
		//read the values
		IDataReader reader = dbcmd.ExecuteReader();

		string textToShow="";

		while (reader.Read ()) {
			textToShow += reader.GetInt32 (0) + " " + reader.GetString(1) + " " + reader.GetInt32(2) + "\n";
		}

		GameObject.Find ("HighScoresText").GetComponent<Text> ().text = textToShow;

		reader.Close ();
		reader = null;
		dbcmd.Dispose();
		dbcmd = null;
	
	}


	void closeDb()
	{

		dbconn.Close();
		dbconn = null;
	}

	//happens when the insert button is pressed
	public void insertButtonPressed(){
		
		IDbCommand dbcmd = dbconn.CreateCommand ();

		string name = GameObject.Find ("NameFieldText").GetComponent<Text> ().text;

		int score = Int32.Parse (GameObject.Find ("ScoreFieldText").GetComponent<Text> ().text);

		string insertQuery = "INSERT INTO scores(playername,score) VALUES('" + name + "'," + score + ")";

		Debug.Log (insertQuery);

		dbcmd.CommandText = insertQuery;


		dbcmd.ExecuteNonQuery();

		getData ();

		//closeDb ();

	}

	
	// Update is called once per frame
	void Update () {
	
	}
}
