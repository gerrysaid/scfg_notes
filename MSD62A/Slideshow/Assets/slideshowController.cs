﻿using UnityEngine;
using System.Collections;

public class slideshowController : MonoBehaviour {

	public string image1Name;
	public string image2Name;
	public string image3Name;
	public string image4Name;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		//GameObject.Find ("Slide1").GetComponent<SpriteRenderer> ().enabled = true;

		if (Input.GetKeyDown (KeyCode.Keypad1)) {
			GameObject.Find (image1Name).GetComponent<SpriteRenderer> ().enabled = true;
			GameObject.Find (image2Name).GetComponent<SpriteRenderer> ().enabled = false;
			GameObject.Find (image3Name).GetComponent<SpriteRenderer> ().enabled = false;
			GameObject.Find (image4Name).GetComponent<SpriteRenderer> ().enabled = false;
		}

		if (Input.GetKeyDown (KeyCode.Keypad2)) {
			GameObject.Find (image1Name).GetComponent<SpriteRenderer> ().enabled = false;
			GameObject.Find (image2Name).GetComponent<SpriteRenderer> ().enabled = true;
			GameObject.Find (image3Name).GetComponent<SpriteRenderer> ().enabled = false;
			GameObject.Find (image4Name).GetComponent<SpriteRenderer> ().enabled = false;
		}

		if (Input.GetKeyDown (KeyCode.Keypad3)) {
			GameObject.Find (image1Name).GetComponent<SpriteRenderer> ().enabled = false;
			GameObject.Find (image2Name).GetComponent<SpriteRenderer> ().enabled = false;
			GameObject.Find (image3Name).GetComponent<SpriteRenderer> ().enabled = true;
			GameObject.Find (image4Name).GetComponent<SpriteRenderer> ().enabled = false;
		}

		if (Input.GetKeyDown (KeyCode.Keypad4)) {
			GameObject.Find (image1Name).GetComponent<SpriteRenderer> ().enabled = false;
			GameObject.Find (image2Name).GetComponent<SpriteRenderer> ().enabled = false;
			GameObject.Find (image3Name).GetComponent<SpriteRenderer> ().enabled = false;
			GameObject.Find (image4Name).GetComponent<SpriteRenderer> ().enabled = true;
		}


	}
}
