﻿using UnityEngine;

using UnityEngine.UI;

using System.Collections;

public class buttonsController : MonoBehaviour {

	int currentPicture = 1;
	int totalPictures = 4;

	//array of pictures
	string[] pictures = new string[4];

	//at the start of the application, set the slide names
	void Start()
	{
		pictures [0] = "Slide1";
		pictures [1] = "Slide2";
		pictures [2] = "Slide3";
		pictures [3] = "Slide4";
		firstButtonPressed ();
	}


	public void resetImages()
	{
		for (int counter = 0; counter < pictures.Length; counter++) {
			GameObject.Find (pictures[counter]).GetComponent<SpriteRenderer> ().enabled = false;
		}
	}

	//triggered when the random button is pressed
	public void randomButtonPressed()
	{

		//this will give me from 0 till 3 (Random.Range works from the index to the value-1)
		int randomIndex = Random.Range (0, pictures.Length);
		Debug.Log (randomIndex);
		resetImages ();
		currentPicture = randomIndex;
		GameObject.Find (pictures[currentPicture]).GetComponent<SpriteRenderer> ().enabled = true;

	}


	//when first button is pressed
	public void firstButtonPressed(){


		//length of the array is pictures.length
		resetImages();

		currentPicture = 0;
		GameObject.Find (pictures[currentPicture]).GetComponent<SpriteRenderer> ().enabled = true;

		GameObject.Find ("pictureNumberText").GetComponent<Text> ().text = currentPicture+1 + "/" + totalPictures;

		Debug.Log ("first");
	}

	public void lastButtonPressed(){

		resetImages ();

		currentPicture = pictures.Length-1;
		GameObject.Find (pictures[currentPicture]).GetComponent<SpriteRenderer> ().enabled = true;


		GameObject.Find ("pictureNumberText").GetComponent<Text> ().text = currentPicture+1 + "/" + totalPictures;

		Debug.Log ("last");
	}

	public void nextButtonPressed(){
		Debug.Log ("next");

		resetImages ();
		if (currentPicture < pictures.Length - 1) {
			currentPicture++;
		} else {
			currentPicture = 0;
		}

		GameObject.Find (pictures[currentPicture]).GetComponent<SpriteRenderer> ().enabled = true;

		GameObject.Find ("pictureNumberText").GetComponent<Text> ().text = currentPicture+1 + "/" + totalPictures;

	}

	public void prevButtonPressed(){
		Debug.Log ("prev");

		resetImages ();
		if (currentPicture > 0) {
			currentPicture--;
		} else {
			currentPicture = pictures.Length-1;
		}

		GameObject.Find (pictures[currentPicture]).GetComponent<SpriteRenderer> ().enabled = true;

		GameObject.Find ("pictureNumberText").GetComponent<Text> ().text = currentPicture+1 + "/" + totalPictures;

	}



}
